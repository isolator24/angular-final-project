import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public redirectToAddNewBranch() {
    this.router.navigate(['branch-add']);
  }

  public redirectToAssignMovieToBranch() {
    this.router.navigate(['admin-branch-movie-assignment']);
  }

  public redirectToAddNewUsers() {
    this.router.navigate(['user-add']);
  }

}
