import { Branch } from './../@core/models/branch.model';
import { BranchService } from './../@core/services/branch.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit, OnDestroy {
  subscription: Subscription = new Subscription();
  branchList: Observable<Branch[]>;
  public selectedBranchId: number;

  constructor(
    private branchService: BranchService,
    private router: Router
    ) { }

    ngOnInit() {
      this.getAll();
    }

    delete(branch: Branch) {
        this.branchService.delete(branch.id);
        this.getAll();
      }

    ngOnDestroy() {
      this.subscription.unsubscribe();
    }

    getAll() {
      this.branchList = this.branchService.branches$;
    }

    redirectTo(id: number) {
       this.router.navigate(['branch/branch-detail', id]);
    }
  }
