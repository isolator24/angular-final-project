import { CheckoutDetailService } from './../@core/services/checkout-detail.service';
import { SharedModule } from './../@shared/shared.module';
import { MovieComponent } from './movie.component';
import { MovieAddComponent } from './movie-add/movie-add.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MovieService } from '../@core/services/movie.service';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    MovieAddComponent,
    MovieComponent,
    MovieDetailComponent
  ],
  providers: [
    MovieService, CheckoutDetailService
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbModule
  ]
})
export class MovieModule { }
