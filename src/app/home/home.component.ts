import { Movie } from './../@core/models/movie.model';
import { MovieService } from './../@core/services/movie.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  movieList: Observable<Movie[]>;
  public searchText: string;

  constructor(
    private movieService: MovieService,
    private router: Router
    ) {
    }

    ngOnInit() {
      this.getAll();
    }

    delete(movie: Movie) {
        this.movieService.delete(movie.id);
        this.getAll();
      }

    ngOnDestroy() {
      this.subscription.unsubscribe();
    }

    getAll() {
      this.movieList = this.movieService.movies$;
    }

    redirectTo(id: number) {
      this.router.navigate(['movie/movie-detail', id]);
    }

    public searchTitle() {
      //if (this.searchText) {
        const searchText = this.searchText ? this.searchText.toLowerCase() : "";
        this.movieService.searchMovies(searchText);
      //}
    }
  }
