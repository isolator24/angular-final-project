import { BranchMovieAssignment } from './../models/branch-movie-assignment.model';
import { Branch } from './../models/branch.model';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class BranchService {
  private readonly _branches = new BehaviorSubject<Branch[]>([]);
  readonly branches$ = this._branches.asObservable();

  private readonly _branchAssignments = new BehaviorSubject<BranchMovieAssignment[]>([]);
  readonly branchAssignments$ = this._branchAssignments.asObservable();

  constructor(private http: HttpClient) {
    this.getAll();
  }

  public get branches(): Branch[] {
    return this._branches.getValue();
  }

  public set branches(branches: Branch[]) {
    this._branches.next(branches);
  }

  public get branchAssignments(): BranchMovieAssignment[] {
    return this._branchAssignments.getValue();
  }

  public set branchAssignments(branchAssignments: BranchMovieAssignment[]) {
    this._branchAssignments.next(branchAssignments);
  }

  public getAllBranchAssignments() {
    return this.http
      .get<BranchMovieAssignment[]>(`${environment.apiConfig.url}/branchMovieAssignment`)
      .pipe(tap(res => res));
  }

  public get(id) {
    return this.http
      .get<Branch>(`${environment.apiConfig.url}/branches/${id}`)
      .pipe(tap(res => res));
  }

  public getBranches() {
    return this.http
      .get<Branch[]>(`${environment.apiConfig.url}/branches`)
      .pipe(tap(res => res));
  }

  public getAll() {
    return this.http
      .get<Branch[]>(`${environment.apiConfig.url}/branches`)
      .pipe(tap(res => res))
      .subscribe((data: Branch[]) => {
        this.branches = data;
      });
  }

  public delete(id) {
    return this.http
      .delete(`${environment.apiConfig.url}/branches/${id}`)
      .subscribe(x => {
        const branches = this._branches
          .getValue()
          .filter(branch => branch.id !== id);

        this.branches = branches;
      });
  }

  public add(branch: Branch) {
    return this.http
      .post(`${environment.apiConfig.url}/branches`, branch)
      .subscribe((data: Branch) => {
        this.branches = [...this.branches, data];
      });
  }

  public addBranchMovieAssignment(branchMovieAssignment: BranchMovieAssignment) {
    return this.http.post(`${environment.apiConfig.url}/branchMovieAssignment`, branchMovieAssignment)
    .subscribe((data: BranchMovieAssignment) => {
      this.branchAssignments = [...this.branchAssignments, data];
    });;
  }
}
