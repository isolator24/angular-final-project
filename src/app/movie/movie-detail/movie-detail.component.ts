import { SeatManagerService } from "./../../@core/services/seat-manager.service";
import { CheckoutDetail } from "./../../@core/models/checkout-detail.model";
import { CheckoutDetailService } from "./../../@core/services/checkout-detail.service";
import { SharedModule } from "./../../@shared/shared.module";
import { SeatStatus } from "./../../@core/constants/seat-status.enum";
import { BranchMovieAssignment } from "./../../@core/models/branch-movie-assignment.model";
import { TimeSlot } from "../../@core/models/time-slot.model";
import { ConvertDurationStringtoTimePipe } from "./../../@shared/pipes/convert-duration-stringto-time.pipe";
import { BranchService } from "./../../@core/services/branch.service";
import { Movie } from "./../../@core/models/movie.model";
import { Branch } from "./../../@core/models/branch.model";
import { MovieService } from "./../../@core/services/movie.service";
import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from "rxjs";
import { Router } from "@angular/router";
import { NgbDate } from "@ng-bootstrap/ng-bootstrap";
import { Duration } from "./../../@core/models/duration.model";
import { Renderer2 } from "@angular/core";
import { SeatManager } from "src/app/@core/models/seat-manager";

@Component({
  selector: "app-movie-detail",
  templateUrl: "./movie-detail.component.html",
  styleUrls: ["./movie-detail.component.css"]
})
export class MovieDetailComponent implements OnInit {
  private subscription: Subscription = new Subscription();
  public movie: Movie;
  public branches: Branch[];
  public selectedDate: NgbDate;
  public duration: Duration;
  public convertDurationStringtoTimePipe: ConvertDurationStringtoTimePipe;
  public durationString: string;
  public timeSlots = [];
  public cinemas = [];
  public selectedTimeSlot: TimeSlot;
  public branchMovieAssign: BranchMovieAssignment[];
  public branchMovieAssignDistinct: BranchMovieAssignment[];
  public selectedBranchId: number;
  public cinemaCount: number;
  public selectedCinemaNumber: number;
  public seatRowA: string[];
  public seatRowB: string[];
  public seatRowC: string[];
  public seatRowE: string[];
  public seatRowF: string[];
  public seatRowG: string[];
  public seatTypeA: number;
  public seatTypeB: number;
  //public isTaken: boolean = SeatStatus.Taken ? true : false ;
  public isTaken: boolean;
  public willBook: boolean;
  public seatsToBook: string[] = [];
  public showSelectedSeats: string;
  public checkoutDetail: CheckoutDetail;
  public selectedBranchName: string;
  public seatManager: SeatManager[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private movieService: MovieService,
    private branchService: BranchService,
    private checkoutDetailService: CheckoutDetailService,
    private seatManagerService: SeatManagerService
  ) {
    this.convertDurationStringtoTimePipe = new ConvertDurationStringtoTimePipe();
  }

  ngOnInit() {
    this.seatTypeA = 6;
    this.seatTypeB = 12;
    this.isTaken = false;
    this.showSelectedSeats = "";

    const today = new Date();
    this.selectedDate = new NgbDate(
      today.getFullYear(),
      today.getMonth() + 1,
      today.getDate()
    );

    const id = this.route.snapshot.paramMap.get("id");

    this.movieService.get(id).subscribe(x => {
      this.movie = x;
      this.getTimeSlots();
    });

    // this.branchService.getBranches().subscribe(x => {
    //   this.branch = x;
    // });
    this.cinemas = [];
    this.branchService.getAllBranchAssignments().subscribe(x => {
      this.branchMovieAssign = x;
      this.fillBranches();
    });

    this.clearSeats();
  }

  public clearSeats() {
    for (let j = 1; j < 6; j++) {
      let totalLoop: number;

      totalLoop = j === 1 ? this.seatTypeA : this.seatTypeB;

      for (let i = 1; i <= totalLoop; i++) {
        let alphabet = this.getAlphabet(j);
        let currentIndex = alphabet + i;
        let id = document.getElementById(currentIndex);

        if (id != null) {
        id.classList.remove("selected");
        }
      }
    }
  }

  public markSeatsTaken() {
    this.clearSeats();

    this.seatManagerService.getSeatManager().subscribe(x => {
      this.seatManager = x;
    });

    if (this.seatManager) {

    this.seatManager.forEach(seatCollection => {
      seatCollection.seats.forEach(z => {
        for (let j = 1; j < 6; j++) {
          let totalLoop: number;
          totalLoop = j === 1 ? this.seatTypeA : this.seatTypeB;
          this.isTaken = false;
          for (let i = 1; i <= totalLoop; i++) {
            let alphabet = this.getAlphabet(j);
            const letter = alphabet;
            let currentIndex = letter + i;
            let id = document.getElementById(currentIndex);

            if (id.id == z ) {
              if (seatCollection.branchId == this.selectedBranchId &&
                  seatCollection.cinemaNumber == this.selectedCinemaNumber &&
                  seatCollection.timeSlot == this.selectedTimeSlot &&
                  seatCollection.movieId == this.movie.id &&
                  seatCollection.date.year == this.selectedDate.year &&
                  seatCollection.date.month == this.selectedDate.month &&
                  seatCollection.date.day == this.selectedDate.day){
                  id.classList.add('selected');
                  this.isTaken = true;
              }
            }
          }
        }
        this.isTaken = false;
      });
    });
  }
}

  public getAllCinemasByBranch() {
    this.cinemas = [];

    let cinemaHolder;
    this.branchMovieAssign.forEach(x => {
      if (x.branchId === this.selectedBranchId) {
        if (cinemaHolder !== x.cinemaNumber) {
          this.cinemas.push(x.cinemaNumber);
        }
        cinemaHolder = x.cinemaNumber;
      }
    });


    this.cinemas.sort();
  }

  public fillBranches() {
    this.branches = [];
    const branchIds = [
      ...new Set(this.branchMovieAssign.map(id => id.branchId))
    ];

    branchIds.forEach(x => {
      this.branchService.get(x).subscribe(b => {
        this.branches.push(b);
        this.cinemaCount = b.cinemaCount;
      });
    });
  }

  public loadCinemas() {
    this.markSeatsTaken();
    this.getAllCinemasByBranch();

    this.branchService.get(this.selectedBranchId).subscribe(b => {
      this.selectedBranchName = b.name;

    });


  }

  public getTimeSlots() {
    if (this.movie !== undefined && this.movie != null) {
      this.durationString = this.movie.duration;

      const n = this.convertDurationStringtoTimePipe.transform(
        this.durationString
      );
      const duration: Duration = new Duration();

      duration.hour = n[0];
      duration.minute = n[1];

      var d = new Date();
      var dayToday = d.getDay();
      d.getDate();
      var s: string;
      d.setHours(10);
      d.setMinutes(0);
      let i: number = 1;
      this.timeSlots = [];
      while (d.getDay() === dayToday) {
        this.timeSlots.push({ id: i, timeSlot: this.timeformat(d) });
        //this.timeSlots.push({label: this.timeformat(d), value: this.timeformat(d)});

        d.setHours(d.getHours() + parseInt(duration.hour.toString()));
        d.setMinutes(d.getMinutes() + parseInt(duration.minute.toString()));
        i++;
      }
    }
  }

  public timeformat(date: Date) {
    let h = date.getHours();
    let m = date.getMinutes();
    let x = h >= 12 ? "PM" : "AM";
    h = h % 12;
    h = h ? h : 12;
    //m = m < 10 ? '0' + m: m;
    let mytime = h + ":" + (m < 10 ? "0" + m : m) + " " + x;
    return mytime;
  }

  public redirectTo() {
    this.checkoutDetail = new CheckoutDetail();
    // id: number;
    // movieTitle : string;
    // movieId: number;
    // branchName: string;
    // branchId: number;
    // screeningDate: Date;
    // timeSlot: string;
    // cinemaNumber: number;
    // totalAmount: number;
    // selectedSeats: string[];
    this.checkoutDetail.branchId = this.selectedBranchId;
    this.checkoutDetail.branchName = this.selectedBranchName;
    this.checkoutDetail.movieTitle = this.movie.title;
    this.checkoutDetail.movieId = this.movie.id;
    this.checkoutDetail.screeningDate = this.selectedDate;
    this.checkoutDetail.timeSlot = this.selectedTimeSlot;
    this.checkoutDetail.cinemaNumber = this.selectedCinemaNumber;
    this.checkoutDetail.totalAmount =
      this.seatsToBook.length * this.movie.price;
    this.checkoutDetail.selectedSeats = this.seatsToBook;

    this.checkoutDetailService
      .addCheckoutDetails(this.checkoutDetail)
      .subscribe((x: CheckoutDetail) => {
        this.checkoutDetail.id = x.id;
        this.router.navigate(["payment", this.checkoutDetail.id]);
      });
  }

  public getDateSelection(date: NgbDate) {
    this.selectedDate = date;

    this.markSeatsTaken();
  }

  public selectedSeat(event) {
    const target = event.target || event.srcElement || event.currentTarget;
    const idAttr = target.attributes.id;
    const value = idAttr.nodeValue;

    const wasChosen = event.srcElement.classList.contains("chosen");

    if (wasChosen) {
      event.srcElement.classList.remove("chosen");
    } else {
      event.srcElement.classList.add("chosen");
    }

    this.seatsToBook = [];
    this.showSelectedSeats = "";

    for (let j = 1; j < 6; j++) {
      let totalLoop: number;

      totalLoop = j === 1 ? this.seatTypeA : this.seatTypeB;

      this.getSeatToBeBooked(totalLoop, this.getAlphabet(j));
    }
  }

  private getAlphabet(iteration: number) {
    let letter: string;
    switch (iteration) {
      case 2:
        letter = "B";
        break;
      case 3:
        letter = "C";
        break;
      case 4:
        letter = "D";
        break;
      case 5:
        letter = "E";
        break;
      default:
      case 1:
        letter = "A";
        break;
    }
    return letter;
  }

  public getSeatToBeBooked(index: number, alphabet: string) {
    for (let i = 1; i <= index; i++) {
      const letter = alphabet;
      let currentIndex = letter + i;
      let id = document.getElementById(currentIndex);

      let isSelected = id.classList.contains("chosen");

      if (isSelected) {
        this.seatsToBook.push(currentIndex);
        let comma = this.seatsToBook.length === 1 ? "" : ", ";
        this.showSelectedSeats = this.showSelectedSeats.concat(
          comma + currentIndex
        ); //.concat(this.seatsToBook.length === 1) ? '' : comma;
      }
    }
  }
}
