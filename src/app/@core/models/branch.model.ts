export class Branch {
  id: number;
  name: string;
  cinemaCount: number;
  address: string;
  branchImage: string;
}
