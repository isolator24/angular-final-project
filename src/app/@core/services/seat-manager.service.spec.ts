import { TestBed } from '@angular/core/testing';

import { SeatManagerService } from './seat-manager.service';

describe('SeatManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeatManagerService = TestBed.get(SeatManagerService);
    expect(service).toBeTruthy();
  });
});
