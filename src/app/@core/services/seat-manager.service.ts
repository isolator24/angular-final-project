import { SeatManager } from './../models/seat-manager';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeatManagerService {
  private readonly _seatManager = new BehaviorSubject<SeatManager[]>([]);
  readonly seatManager$ = this._seatManager.asObservable();

  constructor(private http: HttpClient) {
    this.getAll();
  }

  public get seatManager(): SeatManager[] {
    return this._seatManager.getValue();
  }

  public set seatManager(seatManager: SeatManager[]) {
    this._seatManager.next(seatManager);
  }

  public get(id) {
    return this.http
      .get<SeatManager>(`${environment.apiConfig.url}/seatManager/${id}`)
      .pipe(tap(res => res));
  }

  public getSeatManager() {
    return this.http
      .get<SeatManager[]>(`${environment.apiConfig.url}/seatManager`)
      .pipe(tap(res => res));
  }

  public getAll() {
    return this.http
      .get<SeatManager[]>(`${environment.apiConfig.url}/seatManager`)
      .pipe(tap(res => res))
      .subscribe((data: SeatManager[]) => {
        this.seatManager = data;
      });
  }

  public addSeatManager(seatManager: SeatManager) {
    return this.http
      .post(`${environment.apiConfig.url}/seatManager`, seatManager, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }).pipe(tap(res => res));
  }

  public updateSeatManager(id, seatManager: SeatManager) {
    return this.http
    .put(`${environment.apiConfig.url}/seatManager/${id}` , seatManager, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).pipe(tap(res => res));
  }
}
