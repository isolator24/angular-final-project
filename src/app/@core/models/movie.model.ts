export class Movie {
  id: number;
  title: string;
  moviePosterUrl: string;
  duration: string;
  rating: string;
  releaseDate: Date;
  viewTrailerUrl: string;
  director: string;
  casts: string[];
  description: string;
  price: number;
}
