import { ConvertDurationStringtoTimePipe } from './@shared/pipes/convert-duration-stringto-time.pipe';
import { AdminModule } from './admin/admin.module';
import { BranchModule } from './branch/branch.module';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MovieModule } from './movie/movie.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './@core/core.module';
import { SharedModule } from './@shared/shared.module';
import { ThemeModule } from './@theme/theme.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaymentComponent, NgbdModalConfirmAutofocus, NgbdModalConfirm } from './payment/payment.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    PaymentComponent,
    ConvertDurationStringtoTimePipe,
    NgbdModalConfirmAutofocus,
    NgbdModalConfirm,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BranchModule,
    CoreModule,
    SharedModule,
    ThemeModule,
    MovieModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    AdminModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  exports: [
    ConvertDurationStringtoTimePipe
  ],
  providers: [
  ],
  entryComponents: [
    NgbdModalConfirmAutofocus, NgbdModalConfirm
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
