import { User } from './../models/user.model';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly _users = new BehaviorSubject<User[]>([]);
  readonly users$ = this._users.asObservable();

  constructor(private http: HttpClient) {
   }

   public get users(): User[] {
    return this._users.getValue();
}

   public set users(users: User[]) {
    this._users.next(users);
}

get(username) {
  return this.http.get<User>(`${environment.apiConfig.url}/movies/${username}`)
    .pipe(tap(res => res));
}
}
