import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'values'
})
export class ValuesPipe implements PipeTransform {

  transform(value): any {
    const values = [];
    const arrayLength = parseInt(value);

    for (let i = 0; i < arrayLength; i++) {
      values.push(value[i]);
    }


    return values;
  }

}
