import { TestBed } from '@angular/core/testing';

import { CheckoutDetailService } from './checkout-detail.service';

describe('CheckoutDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheckoutDetailService = TestBed.get(CheckoutDetailService);
    expect(service).toBeTruthy();
  });
});
