import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Movie } from './../../@core/models/movie.model';
import { MovieService } from '../../@core/services/movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {

  myForm: FormGroup;
  formData: Movie;

  constructor(
    private fb: FormBuilder,
    private movieService: MovieService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.myForm = this.fb.group({
      title: ['', Validators.required],
      moviePosterUrl: ['', Validators.required],
      duration: ['', Validators.required],
      rating: ['', Validators.required],
      releaseDate: ['', Validators.required],
      viewTrailerUrl: ['', Validators.required],
      director: ['', Validators.required],
    });
  }

  onSubmit() {
    this.formData = this.myForm.value;
    this.movieService.add(this.formData);

    this.router.navigate(['admin']);
}

}
