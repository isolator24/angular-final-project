import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PaymentComponent } from './payment/payment.component';
import { AuthenticationGuard } from './@core/guards/authentication.guard';
import { AdminComponent } from './admin/admin.component';
import { AdminBranchMovieAssignmentComponent } from './admin/admin-branch-movie-assignment/admin-branch-movie-assignment.component';
import { BranchComponent } from './branch/branch.component';
import { BranchAddComponent } from './branch/branch-add/branch-add.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MovieAddComponent } from './movie/movie-add/movie-add.component';
import { MovieDetailComponent } from './movie/movie-detail/movie-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchDetailComponent } from './branch/branch-detail/branch-detail.component';

const routes: Routes = [

  { path: 'admin', component: AdminComponent },
  { path: 'admin-branch-movie-assignment', component: AdminBranchMovieAssignmentComponent },
  { path: 'branch', component: BranchComponent },
  { path: 'branch-add', component: BranchAddComponent },
  { path: 'branch/branch-detail/:id', component: BranchDetailComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'movie-add', component: MovieAddComponent },
  { path: 'movie/movie-detail/:id', component: MovieDetailComponent },
  { path: 'payment', component: PaymentComponent, canActivate: [AuthenticationGuard] },
  { path: 'payment/:id', component: PaymentComponent, canActivate: [AuthenticationGuard] },
  { path: '', component: HomeComponent },
  { path: '**', component: PageNotFoundComponent }, // todo: page not found


   // { path: 'shop', loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthenticationGuard]
})
export class AppRoutingModule { }
