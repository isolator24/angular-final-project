import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'alphabeticallySorting'
})
export class AlphabeticallySortingPipe implements PipeTransform {

    /*
     *  Default Angular pipe transform method
     *
     *  @params:
     *      values - list of values (primitives or objects)
     *      args - AlphabetSortArgs object property name string and descending flag
     *
     *  @return:
     *      sorted list of items
     */
    transform(items: Array<any>, args?: AlphabetSortArgs): any {
        if (!items) {
            return null;
        }

        let direction: number = args && args.descending ? -1 : 1; // sort direction

        let propertyName: string = args && args.property ? String(args.property) : null;

        return items.sort((a, b) => {
            let first: string = this.getValue(a, propertyName);
            let second: string = this.getValue(b, propertyName);

            if (first < second) {
                return -1 * direction;
            }
            else if (first > second) {
                return 1 * direction;
            }
            else {
                return 0;
            }

        });
    }

    /*
     *  Helper function to get the value of an item with or with out property name
     *
     *  @params:
     *      item - item to get value of (object or primitive)
     *      propName - string property name as determined in transform
     *
     *  @return:
     *      string value of given item + propName combination
     *
     */
    private getValue(item: any, propertyName: string): string {

        if (!item) {
            return '';
        }

        if (item instanceof Object) {
            if (!propertyName) {
                return '';
            }

            return item[propertyName] ? String(item[propertyName]).toLowerCase() : '';
        }
        else {
            return String(item).toLowerCase();
        }
    }

}

/*
 * Class for typing the arguments for this pipe
 *
 * string property name for when objects are being sorted
 * boolean descending flag for when we want the sort order to be descending
 */
export class AlphabetSortArgs {
    public property?: string;
    public descending?: boolean;

    public constructor() { }
}
