import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BranchComponent } from './branch.component';
import { BranchAddComponent } from './branch-add/branch-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BranchService } from '../@core/services/branch.service';
import { BranchDetailComponent } from './branch-detail/branch-detail.component';

@NgModule({
  declarations: [
    BranchComponent,
    BranchAddComponent,
    BranchDetailComponent
  ],
  providers: [
    BranchService
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class BranchModule { }
