import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public isLogin() {
    return localStorage.getItem('isLoggedIn') === 'true';
  }

  public loggedinUser() {
    return localStorage.getItem('token');
  }
}
