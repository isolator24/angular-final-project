export class BranchMovieAssignment {
  id: number;
  branchId: number;
  cinemaNumber: number;
  movieId: number;
  createdBy: number;
}
