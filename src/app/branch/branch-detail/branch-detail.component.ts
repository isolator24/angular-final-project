import { MovieService } from './../../@core/services/movie.service';
import { Movie } from './../../@core/models/movie.model';
import { ActivatedRoute } from "@angular/router";
import { BranchMovieAssignment } from "./../../@core/models/branch-movie-assignment.model";
import { BranchService } from "./../../@core/services/branch.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-branch-detail",
  templateUrl: "./branch-detail.component.html",
  styleUrls: ["./branch-detail.component.css"]
})
export class BranchDetailComponent implements OnInit {
  subscription: Subscription = new Subscription();
  //perBranchList: Observable<Branch[]>;
  branchAssignment: BranchMovieAssignment[];
  filteredBranchByBranchId: BranchMovieAssignment[] = [];
  movie: Movie[] = [];
  cinema: number[] = [];
  selectedBranchName: string;

  constructor(
    private branchService: BranchService,
    private router: Router,
    private route: ActivatedRoute,
    private movieService: MovieService
  ) {}

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get("id");

    this.branchService.getAllBranchAssignments().subscribe(x => {
      this.branchAssignment = x.filter(x => x.branchId == parseInt(id));
      this.branchAssignment = this.branchAssignment.filter((el, i, a) => i === a.indexOf(el));

      this.branchService.get(id).subscribe(b => {
        this.selectedBranchName = b.name;
      });

      let movie;
      let cinema;
      this.branchAssignment.forEach(x => {

        if (movie != x.movieId && cinema != x.cinemaNumber) {
          this.filteredBranchByBranchId.push(x);
        }
        movie = x.movieId;
        cinema = x.cinemaNumber;
      });

      this.filteredBranchByBranchId.forEach(m => {
          this.movieService.get(m.movieId).subscribe(x => {
            this.movie.push(x);

            this.cinema.push(m.cinemaNumber);
          });
      });

    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAll() {}

  redirectTo(id: number) {
     this.router.navigate(['movie/movie-detail', id]);
  }
}
