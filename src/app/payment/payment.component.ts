import { SeatManagerService } from './../@core/services/seat-manager.service';
import { SeatManager } from './../@core/models/seat-manager';
import { CheckoutDetail } from './../@core/models/checkout-detail.model';
import { ActivatedRoute } from '@angular/router';
import { CheckoutDetailService } from './../@core/services/checkout-detail.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { TimeSlot } from '../@core/models/time-slot.model';
import { Router } from '@angular/router';
import { SeatStatus } from '../@core/constants/seat-status.enum';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  public checkoutDetail: CheckoutDetail;
  public modal: NgbActiveModal;
  withAutofocus = `<button type="button" ngbAutofocus class="btn btn-danger"
  (click)="modal.close('Ok click')">Ok</button>`;

  constructor(
    private checkoutDetailService: CheckoutDetailService,
    private route: ActivatedRoute,
    private _modalService: NgbModal
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.checkoutDetailService.get(id).subscribe((x: CheckoutDetail) => {
      this.checkoutDetail = x;
    });
  }

  open(name: string) {
    const modalRef = this._modalService.open(MODALS[name]);
    modalRef.componentInstance.movieTitle = this.checkoutDetail.movieTitle;
    modalRef.componentInstance.branchName = this.checkoutDetail.branchName;
    modalRef.componentInstance.screeningDate = this.checkoutDetail.screeningDate;
    modalRef.componentInstance.timeSlot = this.checkoutDetail.timeSlot;
    modalRef.componentInstance.cinemaNumber = this.checkoutDetail.cinemaNumber;
    modalRef.componentInstance.seats = this.checkoutDetail.selectedSeats;
    modalRef.componentInstance.amount = this.checkoutDetail.totalAmount;
    modalRef.componentInstance.id = this.checkoutDetail.id;
    modalRef.componentInstance.branchId = this.checkoutDetail.branchId;
    modalRef.componentInstance.movieId = this.checkoutDetail.movieId;
  }
}

@Component({
  selector: 'ngbd-modal-confirm',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-title">Congratulations</h4>
      <button
        type="button"
        class="close"
        aria-describedby="modal-title"
        (click)="modal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p><strong>Payment was successful.</strong></p>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-danger"
        (click)="modal.close('Ok click')"
      >
        Ok
      </button>
    </div>
  `
})
export class NgbdModalConfirm {
  constructor(public modal: NgbActiveModal) {}
}

@Component({
  selector: "ngbd-modal-confirm-autofocus",
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-title">Payment confirmation</h4>
      <button
        type="button"
        class="close"
        aria-label="Close button"
        aria-describedby="modal-title"
        (click)="modal.dismiss('Cross click')"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>
        <strong
          ><span class="text-danger"
            >Kindly confirm your transaction summary:
          </span></strong
        >
      </p>
      <p><strong>Movie:</strong> {{ movieTitle }}</p>
      <p><strong>Branch:</strong>{{ branchName }}</p>
      <p>
        <strong>Screening date:</strong>
        {{
          (screeningDate.month | convertToMonthString) +
            " " +
            screeningDate.day +
            ", " +
            screeningDate.year
        }}
      </p>
      <p><strong>Time:</strong>{{ timeSlot }}</p>
      <p><strong>Cinema number:</strong> {{ cinemaNumber }}</p>
      <p><strong>Seats:</strong> {{ seats }}</p>
      <h1>Total Amount:{{ amount }}</h1>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-outline-secondary"
        (click)="modal.dismiss('cancel click')"
      >
        Cancel
      </button>
      <button
        type="button"
        ngbAutofocus
        class="btn btn-danger"
        (click)="modal.close('Ok click'); paymentSuccessful()"
      >
        Pay
      </button>
    </div>
  `
})
export class NgbdModalConfirmAutofocus implements OnInit {
  @Input() movieTitle: string;
  @Input() branchName: string;
  @Input() screeningDate: NgbDate;
  @Input() timeSlot: TimeSlot;
  @Input() amount: number;
  @Input() cinemaNumber: number;
  @Input() seats: string[];
  @Input() id: number;
  @Input() branchId: number;
  @Input() movieId: number;

  public checkoutDetail: CheckoutDetail;
  public seatManager: SeatManager;

  @Input() totalAmount;

  constructor(
    public modal: NgbActiveModal,
    private checkoutDetailService: CheckoutDetailService,
    private _modalService: NgbModal,
    private router: Router,
    private seatManagerService: SeatManagerService
  ) {}

  ngOnInit() {}

  public paymentSuccessful() {
    this.checkoutDetail = new CheckoutDetail();
    this.checkoutDetail.branchName = this.branchName;
    this.checkoutDetail.movieTitle = this.movieTitle;
    this.checkoutDetail.screeningDate = this.screeningDate;
    this.checkoutDetail.timeSlot = this.timeSlot;
    this.checkoutDetail.cinemaNumber = this.cinemaNumber;
    this.checkoutDetail.totalAmount = this.amount;
    this.checkoutDetail.selectedSeats = this.seats;
    this.checkoutDetail.branchId = this.branchId;
    this.checkoutDetail.movieId = this.movieId;
    this.checkoutDetail.id = this.id;

    this.checkoutDetail.paymentStatus = "Success";

    this.checkoutDetailService
      .updateCheckoutDetail(this.id, this.checkoutDetail)
      .subscribe(() => {
        this.router.navigate(["home"]);
        this.saveToSeatManager();
        this._modalService.open(
          this._modalService.open(this._modalService.open(MODALS["focusFirst"]))
        );
      });
  }

  public saveToSeatManager() {

      this.seatManager = new SeatManager();
      this.seatManager.seatStatus = SeatStatus.Taken;
      this.seatManager.branchId = this.branchId;
      this.seatManager.cinemaNumber = this.cinemaNumber;
      this.seatManager.timeSlot = this.timeSlot;
      this.seatManager.date = this.screeningDate;
      this.seatManager.movieId = this.movieId;
      this.seatManager.paymentId = this.id;
      this.seatManager.seats = this.seats;

      this.seatManagerService
        .addSeatManager(this.seatManager)
        .subscribe(() => {});
    }
}

const MODALS = {
  focusFirst: NgbdModalConfirm,
  autofocus: NgbdModalConfirmAutofocus
};
