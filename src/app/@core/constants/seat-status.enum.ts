export enum SeatStatus {
  Available = 1,
  Taken = 2,
  Selected = 3
}
