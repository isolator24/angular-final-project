import { AlphabeticallySortingPipe } from './../../@shared/pipes/alphabetically-sorting.pipe';
import { Movie } from './../models/movie.model';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private readonly _movies = new BehaviorSubject<Movie[]>([]);
  readonly movies$ = this._movies.asObservable();
  private alphaSortPipe: AlphabeticallySortingPipe;

  constructor(private http: HttpClient) {
      this.getAll();
      this.alphaSortPipe = new AlphabeticallySortingPipe();

   }

   public get movies(): Movie[] {
    return this._movies.getValue();
}

   public set movies(movies: Movie[]) {
    this._movies.next(movies);
}


  get(id) {
    return this.http.get<Movie>(`${environment.apiConfig.url}/movies/${id}`)
      .pipe(tap(res => res));
  }

  getAll() {
    return this.http.get<Movie[]>(`${environment.apiConfig.url}/movies`)
    .pipe(tap(res => res)).subscribe((data: Movie[]) => {
        this.movies = this.alphaSortPipe.transform(data, { property: 'title'});
    });
  }

  getMovies() {
    return this.http.get<Movie[]>(`${environment.apiConfig.url}/movies`)
      .pipe(tap(res => res));
  }

  public searchMovies(searchText) {
    if (searchText){
      return this.http.get<Movie[]>(`${environment.apiConfig.url}/movies`)
      .pipe(tap(res => res)).subscribe((data: Movie[]) => {
        this.movies = this.alphaSortPipe.transform(data.filter(x => x.title.toLowerCase().includes(searchText)), { property: 'title'});
      });
      }
      else {
        this.getAll();
      }

  }

  delete(id) {
    return this.http.delete(`${environment.apiConfig.url}/movies/${id}`)
    .subscribe(x => {
        const movies = this._movies.getValue().filter(
          movie => movie.id !== id
        );

        this.movies = movies;
    });
}

  add(movie: Movie) {
    return this.http.post(`${environment.apiConfig.url}/movies`, movie)
    .subscribe((data: Movie) => {
        this.movies = [
            ...this.movies,
            data
        ];
    });
}

}
