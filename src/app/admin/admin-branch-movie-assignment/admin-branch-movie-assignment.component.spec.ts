import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminBranchMovieAssignmentComponent } from './admin-branch-movie-assignment.component';

describe('AdminBranchMovieAssignmentComponent', () => {
  let component: AdminBranchMovieAssignmentComponent;
  let fixture: ComponentFixture<AdminBranchMovieAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminBranchMovieAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminBranchMovieAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
