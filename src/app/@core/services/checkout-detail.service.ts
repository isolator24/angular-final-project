import { CheckoutDetail } from "./../models/checkout-detail.model";

import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { tap } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class CheckoutDetailService {
  private readonly _checkoutDetails = new BehaviorSubject<CheckoutDetail[]>([]);
  readonly checkoutDetail$ = this._checkoutDetails.asObservable();

  constructor(private http: HttpClient) {
    this.getAll();
  }

  public get checkoutDetails(): CheckoutDetail[] {
    return this._checkoutDetails.getValue();
  }

  public set checkoutDetails(checkoutDetails: CheckoutDetail[]) {
    this._checkoutDetails.next(checkoutDetails);
  }

  public getAllBranchAssignments() {
    return this.http
      .get<CheckoutDetail[]>(`${environment.apiConfig.url}/checkoutDetails`)
      .pipe(tap(res => res));
  }

  public get(id) {
    return this.http
      .get<CheckoutDetail>(`${environment.apiConfig.url}/checkoutDetails/${id}`)
      .pipe(tap(res => res));
  }

  public getBranches() {
    return this.http
      .get<CheckoutDetail[]>(`${environment.apiConfig.url}/checkoutDetails`)
      .pipe(tap(res => res));
  }

  public getAll() {
    return this.http
      .get<CheckoutDetail[]>(`${environment.apiConfig.url}/checkoutDetails`)
      .pipe(tap(res => res))
      .subscribe((data: CheckoutDetail[]) => {
        this.checkoutDetails = data;
      });
  }

  public addCheckoutDetails(checkoutDetail: CheckoutDetail) {
    return this.http
      .post(`${environment.apiConfig.url}/checkoutDetails`, checkoutDetail, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      }).pipe(tap(res => res));
  }

  public updateCheckoutDetail(id, checkoutDetail: CheckoutDetail) {
    return this.http
    .put(`${environment.apiConfig.url}/checkoutDetails/${id}` , checkoutDetail, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).pipe(tap(res => res));
  }
}
