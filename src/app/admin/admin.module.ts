import { SharedModule } from './../@shared/shared.module';

import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminBranchMovieAssignmentComponent } from './admin-branch-movie-assignment/admin-branch-movie-assignment.component';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AdminComponent,
    AdminBranchMovieAssignmentComponent,
  ],
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
})
export class AdminModule { }
