import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { TimeSlot } from './time-slot.model';

export class CheckoutDetail {
  id: number;
  movieTitle : string;
  movieId: number;
  branchId: number;
  branchName: string;
  screeningDate: NgbDate;
  timeSlot: TimeSlot;
  cinemaNumber: number;
  totalAmount: number;
  selectedSeats: string[];
  paymentStatus: string;
}
