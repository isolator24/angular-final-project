import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "convertToMonthString"
})
export class ConvertToMonthStringPipe implements PipeTransform {
  transform(value: number): any {
    let n;
    var month = new Array();
    month[1] = "January";
    month[2] = "February";
    month[3] = "March";
    month[4] = "April";
    month[5] = "May";
    month[6] = "June";
    month[7] = "July";
    month[8] = "August";
    month[9] = "September";
    month[10] = "October";
    month[11] = "November";
    month[12] = "December";
    return  n = month[value];
  }
}
