import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { SeatStatus } from '../constants/seat-status.enum';
import { TimeSlot } from './time-slot.model';
export class SeatManager {
  id: number;
  name: string[];
  seatStatus: SeatStatus;
  branchId: number;
  cinemaNumber: number;
  timeSlot: TimeSlot;
  date: NgbDate;
  movieId: number;
  paymentId: number;
  seats: string[];
}
