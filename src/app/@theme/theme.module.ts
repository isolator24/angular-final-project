import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default/default.component';
import { MainNavigationComponent } from './default/main-navigation/main-navigation.component';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [DefaultComponent, MainNavigationComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    DefaultComponent,
    RouterModule
  ]
})
export class ThemeModule { }
