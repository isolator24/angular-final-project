import { ValuesPipe } from './pipes/values.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlphabeticallySortingPipe } from './pipes/alphabetically-sorting.pipe';
import { ConvertToMonthStringPipe } from './pipes/convert-to-month-string.pipe';
@NgModule({
  declarations: [AlphabeticallySortingPipe, ConvertToMonthStringPipe, ValuesPipe],
  imports: [
    CommonModule
  ],
  exports: [
    AlphabeticallySortingPipe,
    ConvertToMonthStringPipe,
    ValuesPipe
  ]
})
export class SharedModule { }
