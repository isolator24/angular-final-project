import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Branch } from './../../@core/models/branch.model';
import { BranchService } from '../../@core/services/branch.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-branch-add',
  templateUrl: './branch-add.component.html',
  styleUrls: ['./branch-add.component.css']
})
export class BranchAddComponent implements OnInit {

  myForm: FormGroup;
  formData: Branch;

  constructor(
    private fb: FormBuilder,
    private branchService: BranchService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.myForm = this.fb.group({
      name: ['', Validators.required],
      cinemaCount: ['', Validators.required],
      address: ['', Validators.required],
      branchImage: ['', Validators.required],
    });
  }

  onSubmit() {
    this.formData = this.myForm.value;
    this.branchService.add(this.formData);

    this.router.navigate(['admin']);
}

}
