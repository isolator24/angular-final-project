import { BranchMovieAssignment } from './../../@core/models/branch-movie-assignment.model';
import { ConvertDurationStringtoTimePipe } from './../../@shared/pipes/convert-duration-stringto-time.pipe';
import { MovieService } from './../../@core/services/movie.service';
import { Component, OnInit } from '@angular/core';
import { BranchService } from './../../@core/services/branch.service';
import { Branch } from './../../@core/models/branch.model';
import { Movie } from './../../@core/models/movie.model';
import { Duration } from './../../@core/models/duration.model';

@Component({
  selector: 'app-admin-branch-movie-assignment',
  templateUrl: './admin-branch-movie-assignment.component.html',
  styleUrls: ['./admin-branch-movie-assignment.component.css']
})
export class AdminBranchMovieAssignmentComponent implements OnInit {

  public branches: Branch[];
  public branch: Branch;
  public selectedBranchId: number;
  public selectedBranchName: string;
  public selectedTotalBranch: number;
  public movies: Movie[];
  public movie: Movie;
  public convertDurationStringtoTimePipe: ConvertDurationStringtoTimePipe;
  public duration: Duration;
  public timeSlots: string[] = [];
  public cinemaCount: number;
  public selectedMovie: Movie;
  public selectedCinema: number;

  private durationString: string;

  constructor(
    private branchService: BranchService,
    private movieService: MovieService
  ){
    this.convertDurationStringtoTimePipe = new ConvertDurationStringtoTimePipe();
  }

  ngOnInit() {
    this.branchService.getBranches().subscribe(bs => {
      this.branches = bs;
    });

    this.movieService.getMovies().subscribe(mv => {
      this.movies = mv;
    });

    this.selectedCinema = 0;
  }

  public onChangeSelection(id){
    this.branchService.get(this.selectedBranchId).subscribe(b => {
      this.branch = b;
      this.selectedBranchName = b.name;
      this.selectedTotalBranch = b.cinemaCount;
    });
  }

  public process(movie) {
    // this.movieService.get(id).subscribe(m => {
    //   this.movie = m;
    // });
    this.movie = movie;

    if (this.movie != undefined && this.movie != null)  {
      this.durationString = this.movie.duration;

      const n = this.convertDurationStringtoTimePipe.transform(this.durationString);
      const duration: Duration = new Duration();

      duration.hour = n[0];
      duration.minute = n[1];

      var d = new Date();
      var dayToday = d.getDay();
      d.getDate();
      var s: string;
      d.setHours(10);
      d.setMinutes(0);

      while (d.getDay() == dayToday) {
        this.timeSlots.push(this.timeformat(d));

        d.setHours(d.getHours() + parseInt(duration.hour.toString()));
        d.setMinutes(d.getMinutes() + parseInt(duration.minute.toString()));
      }

    }

  }

  public timeformat(date: Date) {
    var h = date.getHours();
    var m = date.getMinutes();
    var x = h >= 12 ? 'PM' : 'AM';
    h = h % 12;
    h = h ? h : 12;
    //m = m < 10 ? '0' + m: m;
    var mytime= h + ':' + (m < 10 ? '0' + m: m) + ' ' + x;
    return mytime;
  }

  public assignMovie() {
    const branchMovieAssignment = new BranchMovieAssignment();
    branchMovieAssignment.branchId = this.selectedBranchId;
    branchMovieAssignment.movieId = this.movie.id;
    branchMovieAssignment.cinemaNumber = this.selectedCinema;
    this.branchService.addBranchMovieAssignment(branchMovieAssignment);
  }

  public getCinemaNumber(cinemaNumber) {
    this.selectedCinema = cinemaNumber;
  }

  public assignMovieToCinema(movie: Movie) {
    this.timeSlots = [];
    this.process(movie);
    this.selectedMovie = movie;
  }





}
