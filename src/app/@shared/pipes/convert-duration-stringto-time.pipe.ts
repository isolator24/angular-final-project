import { Pipe, PipeTransform } from '@angular/core';
import { Duration } from './../../@core/models/duration.model';
@Pipe({
  name: 'convertDurationStringtoTime'
})
export class ConvertDurationStringtoTimePipe implements PipeTransform {


  transform(value: any): Duration {
    var duration = value.match(/[+-]?\d+(?:\.\d+)?/g);
    return duration;
  }

}
